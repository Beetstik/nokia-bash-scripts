#!/bin/bash
#Davis Jackson
#2020-11-17

if [[ $# -eq 1 ]]; then
        ssh root@$1 "(df -h / && du -shc /usr/local/$1/results/*)"

elif [[ $# -eq 0 ]]; then
        echo "This script is used when you want to check the size of the results folder on a testbed, usually because of a hard drive capacity issue reported by nagion..."
        echo "."
        echo "."
        echo "."
        echo "what is the name of the hard drive you want to check?: "
        read tb_name
        ssh root@$tb_name "(df -h / && du -shc /usr/local/$tb_name/results/*)"
fi