#---------------------------------------------------------------------------------------------------------------------------------------------------
#       Description     :       Run a sanity check on a specified testbed
#       Author          :       Davis Jackson
#       Creation Date   :       May.28/2020
#       Last Edited     :       September.21/2020

if [[ $# -eq 3 ]]; then
        read -n1 -p "Testbed name=$1, Image Version=$2, Platform=$3, is this information correct? [y/n]: " confirmation
        echo ""
        case $confirmation in
                y|Y) regress -testbed $1 -forcePause "Sanity Run Completed. Check Results. If Sanity Passed, Unpause bed." -exitOnFailure True -runSuite Sanity -useimages $2 -platform $3 -priority P0 ;;
                n|N) echo "Ok, quitting..." ;;
                *) echo "invalid option, quitting" ;;
        esac

elif [[ $# -eq 2 ]]; then
        echo "What is the platform that you want to use? [77xx]: "
        read -e platform
        read -n1 -p "Testbed name=$1, Image Version=$2, Platform=$platform, is this information correct? [y/n]: " confirmation
        echo ""
        case $confirmation in
                y|Y) regress -testbed $1 -forcePause "Sanity Run Completed. Check Results. If Sanity Passed, Unpause bed." -exitOnFailure True -runSuite Sanity -useimages $2 -platform $platform -priority P0 ;;
                n|N) echo "Ok, quitting..." ;;
                *) echo "invalid option, quitting" ;;
        esac

elif [[ $# -eq 1 ]]; then
        echo "What is the image version that you want to use? [x.x/y]: "
        read  -e imageVersion
        echo "What is the platform that you want to use? [77xx]: "
        read  -e platform
        echo ""
        read -n1 -p "Testbed name=$1, Image Version=$imageVersion, Platform=$platform, is this information correct?
[y/n]: " confirmation
        echo ""
        case $confirmation in
                y|Y) regress -testbed $1 -forcePause "Sanity Run Completed. Check Results. If Sanity Passed, Unpause bed." -exitOnFailure True -runSuite Sanity -useimages $imageVersion -platform $platform -priority P0 ;;
                n|N) echo "Ok, quitting..." ;;
                *) echo "invalid option, quitting" ;;
        esac

elif [[ $# -eq 0 ]]; then
        echo "What is the name of the testbed that you want to run a sanity check on?: "
        read  -e tbname
        echo ""
        echo "What is the image version that you want to use? [x.x/y]: "
        read  -e imageVersion
        echo "What is the platform that you want to use? [77xx]: "
        read  -e platform
        echo ""
        read -n1 -p "Testbed name=$tbname, Image Version=$imageVersion, Platform=$platform, is this information correct? [y/n]: " confirmation
        echo ""
        case $confirmation in
                y|Y) regress -testbed $tbname -forcePause "Sanity Run Completed. Check Results. If Sanity Passed, Unpause bed." -exitOnFailure True -runSuite Sanity -useimages $imageVersion -platform $platform -priority P0 ;;
                n|N) echo "Ok, quitting..." ;;
                *) echo "invalid option, quitting" ;;
        esac

elif [[ $3 -ne 3 ]] && [[ $# -ne 2 ]] && [[ $# -ne 1 ]] && [[ $# -ne 0 ]]; then
        echo ""
        echo "Illlegal number of parameters, quitting..."
fi