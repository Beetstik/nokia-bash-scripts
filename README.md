# Nokia BASH Scripts

A collection of the BASH scripts I created when I worked at Nokia as a lab technician. These scripts are used to automate tasks I perform daily in order to save time.