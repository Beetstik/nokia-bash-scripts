cwd="$PWD"

echo "Executing git pull for '~/workspace/data' repository..."
cd ~/workspace/data/
git pull
echo ""

echo "Executing git pull for '~/workspace/gash' repository..."
cd ~/workspace/gash/
git pull
echo ""

echo "Executing git pull for '~/workspace/infra' repository..."
cd ~/workspace/infra
git pull
echo ""

echo "Executing git pull for '~/workspace/qa-hiera-yamls' repository..."
cd ~/workspace/qa-hiera-yamls/
git pull
echo ""

echo "Executing git pull for '~/workspace/regress' repository..."
cd ~/workspace/regress/
git pull
echo ""

echo "Executing git pull for '~/workspace/tools' repository..."
cd ~/workspace/tools/
git pull
echo ""

cd $cwd