function whoowns {
        if [[ $# -eq 1 ]]; then
                grep -q "$1" ~/workspace/regress/regress.params | grep -q "OWNER"
                accessType=$(grep "$1" ~/workspace/regress/regress.params | grep "ACCESS" | awk '{print $3}')
                echo $?
                echo "$1 is a $accessType bed, owned by: "
                grep "$1" ~/workspace/regress/regress.params | grep "OWNER" | awk '{print $3,$4}'
                grep "$1" ~/workspace/regress/regress.params | grep "DESCRIPTION"


        echo ""

        elif [[ $# -ne 1 ]] && [[ $# -ne 0 ]]; then
                echo "invalid number of parameters..."
                echo "whoowns.sh can currently only handle 1 testbed option at a time..."
                echo "quitting..."

        fi
}